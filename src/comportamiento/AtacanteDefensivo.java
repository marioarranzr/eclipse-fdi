package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;

public class AtacanteDefensivo extends Comportamiento {

	private static int estado = atacantedefensivo;
	private boolean primeravez;

	public AtacanteDefensivo() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("AtacanteDefensivo");
		myRobotAPI.setSpeed(1);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("AtacanteDefensivo");

	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		myRobotAPI.setBehindBall(myRobotAPI.getOpponentsGoal());
		if (myRobotAPI.canKick() || myRobotAPI.goalkeeperBlocked()) {
			myRobotAPI.kick();
		}
		if(myRobotAPI.getBall().r > 0.2)
		{
			maquina.setEstadoSiguiente(presionardefensivo);
		}
		return myRobotAPI.ROBOT_OK;
	}

	public Estado getMaquina() {
		return maquina;
	}
}
