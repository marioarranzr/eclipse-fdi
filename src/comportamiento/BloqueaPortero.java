package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;
import EDU.gatech.cc.is.util.*;

public class BloqueaPortero extends Comportamiento {

	private static int estado = bloqueaPortero;
	private boolean primeravez;
	
	public BloqueaPortero() {
		super(estado);
	}

	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("BloqueaPortero");
		myRobotAPI.setSteerHeading(myRobotAPI.getOpponentsGoal().t);
		myRobotAPI.setSpeed(1);
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("BloqueaPortero");

	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		Vec2[] x = myRobotAPI.getOpponents();
		Vec2 opponentsGoalKeeper = myRobotAPI.closestTo(x, myRobotAPI.getOpponentsGoal());
		myRobotAPI.setSteerHeading(opponentsGoalKeeper.t);
		return RobotAPI.ROBOT_OK;
	}

	public Estado getMaquina() {
		return maquina;
	}
}
