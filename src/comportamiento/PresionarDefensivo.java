package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;

public class PresionarDefensivo extends Comportamiento {

	private static int estado = presionardefensivo;
	private boolean primeravez;

	public PresionarDefensivo() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("PresionarDefensivo");
		myRobotAPI.setSpeed(1);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("PresionarDefensivo");

	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		myRobotAPI.setBehindBall(myRobotAPI.getOpponentsGoal());
		if (myRobotAPI.getBall().r < 0.1)
		{
			maquina.setEstadoSiguiente(atacantedefensivo);
		}
		return myRobotAPI.ROBOT_OK;
	}
	public Estado getMaquina()
	{
		return maquina;
	}
}
