package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;
import EDU.gatech.cc.is.util.*;

public class DefensaIzquierda extends Comportamiento {

	private static int estado = defensaizquierda;
	private boolean primeravez;
	private boolean comoParar;

	public DefensaIzquierda() {
		super(estado);
		primeravez=true;
	}

	@Override
	public void configure() {
		// TODO Auto-generated method stub

	}

	@Override
	public void end() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("MantenerPosDefIzq");
		if (myRobotAPI.getOurGoal().x > 0) {
			comoParar = condicionParada(myRobotAPI.getPosition().x, 0.50);
		} else {
			comoParar = condicionParada(myRobotAPI.getPosition().x, -0.50);
		}
		myRobotAPI.setSpeed(1);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		// TODO Auto-generated method stub
		arg0.setDisplayString("MantenerPosDefIzq");
	}

	@Override
	public int takeStep() {
		if (primeravez) onInit(myRobotAPI);
		
		if (myRobotAPI.blocked()) myRobotAPI.setBehindBall(myRobotAPI.getOpponentsGoal());
		
		if (myRobotAPI.getOurGoal().x > 0) {
			Vec2 posicion = myRobotAPI.toEgocentricalCoordinates(new Vec2(0.50,-0.4));
			myRobotAPI.setSteerHeading(posicion.t);
			if (comoParar) {
				if (myRobotAPI.getPosition().x > 0.45) {
					myRobotAPI.setSpeed(0);
					maquina.setEstadoSiguiente(moverseizq);
					myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				}
			} else {
				if (myRobotAPI.getPosition().x < 0.55) {
					myRobotAPI.setSpeed(0);
					maquina.setEstadoSiguiente(moverseizq);
					myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				}
			}
		} else {
			Vec2 posicion = myRobotAPI.toEgocentricalCoordinates(new Vec2(-0.50,-0.4));
			myRobotAPI.setSteerHeading(posicion.t);
			if (comoParar) {
				if (myRobotAPI.getPosition().x > -0.55) {
					myRobotAPI.setSpeed(0);
					maquina.setEstadoSiguiente(moverseizq);
					myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				}
			} else {
				if (myRobotAPI.getPosition().x < -0.45) {
					myRobotAPI.setSpeed(0);
					maquina.setEstadoSiguiente(moverseizq);
					myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				}
			}
		}
		return myRobotAPI.ROBOT_OK;
	}

	public Estado getMaquina() {
		return maquina;
	}
	
	public boolean condicionParada(double posJugador, double posPosicion){
		if (posJugador < posPosicion){
			return true;
		} else {
			return false;
		}
	}
}
