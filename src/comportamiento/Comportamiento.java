package comportamiento;

import estado.Estado;
import teams.ucmTeam.Behaviour;
import teams.ucmTeam.RobotAPI;

public class Comportamiento extends Behaviour {

	public static double Pi = Math.PI;

	Estado maquina;
	
	long tiempo;
	int golesFavor;
	int golesContra;
	long tiempoRestante;
	
	public final static int atacanteofensivo = 0;
	public final static int despejarcen = 1;
	public final static int atacantedefensivo = 2;
	public final static int enporteria = 3;
	public final static int defensacentral = 4;
	public final static int defensaderecha = 5;
	public final static int defensaizquierda = 6;
	public final static int moversecen = 7;
	public final static int presionardefensivo = 8;
	public final static int presionarofensivo = 9;
	public final static int quieto = 10;
	public final static int colocarportero = 11;
	public final static int despejarportero = 12;
	public final static int moversedrc = 13;
	public final static int moverseizq =  14;
	public final static int despejardrc = 15;
	public final static int despejarizq = 16;
	public final static int bloqueaPortero = 17;

	public Comportamiento(int i) {
		maquina = new Estado(i);
	}

	@Override
	public void configure() {
		// TODO Auto-generated method stub

	}

	@Override
	public void end() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInit(RobotAPI arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRelease(RobotAPI arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public int takeStep() {
		// TODO Auto-generated method stub
		return 0;
	}

}
