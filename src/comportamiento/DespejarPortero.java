package comportamiento;

import estado.Estado;
import teams.ucmTeam.RobotAPI;

public class DespejarPortero extends Comportamiento {

	private static int estado = despejarportero;
	private boolean primeravez;

	public DespejarPortero() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("DespejarPortero");
		myRobotAPI.setSpeed(0);
		myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("DespejarPortero");
	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		
		if (myRobotAPI.getOurGoal().r > 0.5){
			maquina.setEstadoSiguiente(enporteria);
		}else {
			myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
			if (myRobotAPI.getBall().r < 0.5){
				myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				myRobotAPI.setSpeed(1);
			}
		}	
		return myRobotAPI.ROBOT_OK;
	}
	
	public Estado getMaquina()
	{
		return maquina;
	}
}
