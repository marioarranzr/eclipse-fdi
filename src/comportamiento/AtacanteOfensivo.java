package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;

public class AtacanteOfensivo extends Comportamiento {

	private static int estado = atacanteofensivo;
	private boolean primeravez;

	public AtacanteOfensivo() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("AtacanteOfensivo");
		myRobotAPI.setSpeed(1);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("AtacanteOfensivo");

	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		myRobotAPI.setBehindBall(myRobotAPI.getOpponentsGoal());
		if (myRobotAPI.canKick() || myRobotAPI.goalkeeperBlocked()) {
			myRobotAPI.kick();
		}
		if(myRobotAPI.getBall().r > 0.2)
		{
			maquina.setEstadoSiguiente(presionarofensivo);
		}
		return myRobotAPI.ROBOT_OK;
	}

	public Estado getMaquina() {
		return maquina;
	}
}
