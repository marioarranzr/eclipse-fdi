package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;

public class EnPorteria extends Comportamiento {

	private static int estado = enporteria;
	private boolean primeravez;
	
	public EnPorteria() {
		super(estado);
		primeravez=true;
	}

	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("EnPorteria");
		myRobotAPI.setSteerHeading(myRobotAPI.getOurGoal().t);
		myRobotAPI.setSpeed(1);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("EnPorteria");

	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
	
		myRobotAPI.setSteerHeading(myRobotAPI.getOurGoal().t);
		if (myRobotAPI.getOurGoal().x> 0) {
			if (myRobotAPI.getPosition().x > 1.14 && Math.abs(myRobotAPI.getPosition().y)<0.01) {
				myRobotAPI.setSpeed(0);
				myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				maquina.setEstadoSiguiente(colocarportero);
			}
		} else {
			if (myRobotAPI.getPosition().x < -1.14 && Math.abs(myRobotAPI.getPosition().y)<0.01) {
				myRobotAPI.setSpeed(0);
				myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				maquina.setEstadoSiguiente(colocarportero);
			}
		}

		return RobotAPI.ROBOT_OK;

	}

	public Estado getMaquina() {
		return maquina;
	}
}
