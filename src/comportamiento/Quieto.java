package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;

public class Quieto extends Comportamiento {

	private static int estado = quieto;
	private boolean primeravez;

	public Quieto() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("Quieto");
		primeravez=false;
		myRobotAPI.setSpeed(0);
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("Quieto");
	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		if (myRobotAPI.getOurGoal().x > 0){
			if (myRobotAPI.getBall().x < 0){
				maquina.setEstadoSiguiente(atacanteofensivo);
			}
		} else {
			if (myRobotAPI.getBall().x > 0){
				maquina.setEstadoSiguiente(atacanteofensivo);
			}
		}
		return myRobotAPI.ROBOT_OK;
	}
	public Estado getMaquina()
	{
		return maquina;
	}
}
