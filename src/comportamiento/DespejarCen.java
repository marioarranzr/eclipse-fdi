package comportamiento;
import estado.Estado;
import teams.ucmTeam.RobotAPI;

public class DespejarCen extends Comportamiento {

	private static int estado = despejarcen;
	private boolean primeravez;

	public DespejarCen() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("DespejarCen");
		myRobotAPI.setSpeed(0);
		primeravez=false;
		myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("DespejarCen");
	}

	@Override
	public int takeStep() {
		if (primeravez) onInit(myRobotAPI);

		myRobotAPI.setBehindBall(myRobotAPI.getOpponentsGoal());
		if (myRobotAPI.getOurGoal().x > 0) {
			if (myRobotAPI.getPosition().x < -0.3 || myRobotAPI.getBall().r > 0.5){
				maquina.setEstadoSiguiente(defensacentral);
				if (myRobotAPI.canKick()) myRobotAPI.kick();
			} else {
				if (myRobotAPI.getBall().r < 0.45){
					myRobotAPI.setSpeed(1);
					myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				}
			}
		} else {
			if (myRobotAPI.getPosition().x > 0.3 || myRobotAPI.getBall().r > 0.5){
				maquina.setEstadoSiguiente(defensacentral);
				if (myRobotAPI.canKick()) myRobotAPI.kick();
				
			} else {
				if (myRobotAPI.getBall().r < 0.45){
					myRobotAPI.setSpeed(1);
					myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
				}
			}
		}
		return 0;
	}
	public Estado getMaquina()
	{
		return maquina;
	}
}
