package comportamiento;

import estado.Estado;
import teams.ucmTeam.*;

public class PresionarOfensivo extends Comportamiento {

	private static int estado = presionarofensivo;
	private boolean primeravez;

	public PresionarOfensivo() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("PresionarOfensivo");
		myRobotAPI.setSpeed(1);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("PresionarOfensivo");
	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		myRobotAPI.setBehindBall(myRobotAPI.getOpponentsGoal());
		if (myRobotAPI.getOurGoal().x >0){
			if (myRobotAPI.getPosition().x > 0){
				maquina.setEstadoSiguiente(quieto);
			} else {
				if (myRobotAPI.getBall().r < 0.1){
					maquina.setEstadoSiguiente(atacanteofensivo);
				} 
			}
		} else {
			if (myRobotAPI.getPosition().x < 0){
				maquina.setEstadoSiguiente(quieto);
			} else {
				if (myRobotAPI.getBall().r < 0.1){
					maquina.setEstadoSiguiente(atacanteofensivo);
				} 
			}
		}
		return myRobotAPI.ROBOT_OK;
	}
	public Estado getMaquina()
	{
		return maquina;
	}
}
