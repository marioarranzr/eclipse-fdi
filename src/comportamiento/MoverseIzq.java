package comportamiento;
import estado.Estado;
import teams.ucmTeam.RobotAPI;

public class MoverseIzq extends Comportamiento{
	
	private static int estado = moverseizq;
	private boolean primeravez;

	public MoverseIzq() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInit(RobotAPI arg0) 
	{
		arg0.setDisplayString("MoverseIzq");
		myRobotAPI.setSpeed(0);
		myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) 
	{
		arg0.setDisplayString("MoverseIzq");
	}

	@Override
	public int takeStep() 
	{
		if (primeravez)onInit(myRobotAPI);
		myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
		if (myRobotAPI.getBall().r < 0.7){	
			maquina.setEstadoSiguiente(despejarizq);
		}
		return myRobotAPI.ROBOT_OK;
	}
	
	public Estado getMaquina()
	{
		return maquina;
	}
}