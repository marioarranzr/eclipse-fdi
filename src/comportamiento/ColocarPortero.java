package comportamiento;

import estado.Estado;
import teams.ucmTeam.RobotAPI;

public class ColocarPortero extends Comportamiento {

	private static int estado = colocarportero;
	private boolean primeravez;

	public ColocarPortero() {
		super(estado);
		primeravez=true;
	}
	
	@Override
	public void configure() {
		// TODO Auto-generated method stub
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onInit(RobotAPI arg0) {
		arg0.setDisplayString("ColocarPortero");
		myRobotAPI.setSpeed(0);
		myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
		primeravez=false;
	}

	@Override
	public void onRelease(RobotAPI arg0) {
		arg0.setDisplayString("ColocarPortero");
	}

	@Override
	public int takeStep() {
		if (primeravez)onInit(myRobotAPI);
		myRobotAPI.setSteerHeading(myRobotAPI.getBall().t);
		if(myRobotAPI.getBall().r < 1 ){
			maquina.setEstadoSiguiente(despejarportero);
		}
		
		return myRobotAPI.ROBOT_OK;
	}
	public Estado getMaquina()
	{
		return maquina;
	}
}