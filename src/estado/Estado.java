package estado;

public class Estado {
	
	private int estadoActual;
	private int estadoSiguiente;
	
	public Estado(int estado)
	{
		estadoActual = estado;
		estadoSiguiente = estado;
	}

	public int getEstadoActual() {
		return estadoActual;
	}

	public void setEstadoActual(int estado) {
		this.estadoActual = estado;
	}
	
	public int getEstadoSiguiente() {
		return estadoSiguiente;
	}

	public void setEstadoSiguiente(int estado) {
		this.estadoSiguiente = estado;
	}
	
	
	

}
