package CBRtxt;



public class TeamDescriptionTxt 
{
	private int porteros;
	private int defensas;
	private int delanterosDef;
	private int delanterosOfe;
	private long tiempo;
	private long tiempoRestante;
	private int golesFavor;
	private int golesContra;
	
	
	
	public TeamDescriptionTxt(int porteros, int defensas, int delanterosDef, int delanterosOfe, long tiempo, long tiempoRestante, int golesFavor, int golesContra)
	{
		this.porteros = porteros;
		this.defensas = defensas;
		this.delanterosDef = delanterosDef;
		this.delanterosOfe = delanterosOfe;
		this.tiempo = tiempo;
		this.tiempoRestante = tiempoRestante;
		this.golesFavor = golesFavor;
		this.golesContra = golesContra;
	}
	
	
	
	public int getPorteros() {
		return porteros;
	}



	public void setPorteros(int porteros) {
		this.porteros = porteros;
	}



	public int getDefensas() {
		return defensas;
	}



	public void setDefensas(int defensas) {
		this.defensas = defensas;
	}

	public int getDelanterosDef() {
		return delanterosDef;
	}



	public void setDelanterosDef(int delanterosDef) {
		this.delanterosDef = delanterosDef;
	}



	public int getDelanterosOfe() {
		return delanterosOfe;
	}



	public void setDelanterosOfe(int delanterosOfe) {
		this.delanterosOfe = delanterosOfe;
	}



	public long getTiempo() {
		return tiempo;
	}



	public void setTiempo(long tiempo) {
		this.tiempo = tiempo;
	}



	public long getTiempoRestante() {
		return tiempoRestante;
	}



	public void setTiempoRestante(long tiempoRestante) {
		this.tiempoRestante = tiempoRestante;
	}



	public int getGolesFavor() {
		return golesFavor;
	}



	public void setGolesFavor(int golesFavor) {
		this.golesFavor = golesFavor;
	}



	public int getGolesContra() {
		return golesContra;
	}



	public void setGolesContra(int golesContra) {
		this.golesContra = golesContra;
	}



	public double similitud(TeamDescriptionTxt d2) 
	{
		double res = 0.01;
		
		/*funcion de similitud para goles (45%)*/
		int dif_goles_d1 = this.golesFavor-this.golesContra; 
		int dif_goles_d2 = d2.golesFavor-d2.golesContra;
		/*Los dos casos empatan o ganan o pierden*/
		if (((dif_goles_d1 == 0) && (dif_goles_d2==0)) || ((dif_goles_d1>0) && (dif_goles_d2>0)) || ((dif_goles_d1<0) && (dif_goles_d2<0))) 
		{
			res = res + 0.45;
		}
		/*(1 gana o 1 pierde) y 2 empate*/
		else if (((dif_goles_d1>0) || (dif_goles_d1>0)) && (dif_goles_d2 == 0))
		{
			res = res + (0.3 - (0.1*Math.abs(dif_goles_d1)));
		}
		/*(2 gana o 2 pierde) y 1 empate*/
		else if (((dif_goles_d2>0) || (dif_goles_d2>0)) && (dif_goles_d1 == 0))
		{
			res = res + (0.3 - (0.1*Math.abs(dif_goles_d2)));
		}
		
		/*funcion de similitud para el tiempo (45%)*/
		double tiempo1 = sacar_parte_tiempo((this.tiempoRestante*100)/this.tiempo);
		double tiempo2 = sacar_parte_tiempo((d2.tiempoRestante*100)/d2.tiempo);
		res = res + (0.45-(0.05*(Math.abs(tiempo1-tiempo2))));
		
		/*funcion de similitud para portero(3%)*/
		if (this.porteros == d2.porteros) res= res+0.03;
		
		/*funcion de similitud para defensas(3%)*/
		res = res + (0.03-(0.01*(Math.abs(this.defensas-d2.defensas))));
		
		/*funcion de similitud para delanteros(3%)*/
		int delanteros1 = this.delanterosDef + this.delanterosOfe;
		int delanteros2 = d2.delanterosDef + d2.delanterosOfe;
		res = res + (0.03-(0.01*(Math.abs(delanteros1-delanteros2))));
		
		return res;
	}


	private int sacar_parte_tiempo(double cuenta) 
	{
		int c = (int)cuenta;
		if (c<10) return 0;
		else if (c<20) return 1;
		else if (c<30) return 2;
		else if (c<40) return 3;
		else if (c<50) return 4;
		else if (c<60) return 5;
		else if (c<70) return 6;
		else if (c<80) return 7;
		else if (c<90) return 8;
		else return 9;
	}

}
