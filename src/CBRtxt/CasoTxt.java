package CBRtxt;



public class CasoTxt {
	private TeamDescriptionTxt descripcion;
	private TeamSolutionTxt solucion;

	public CasoTxt(TeamDescriptionTxt descripcion, TeamSolutionTxt solucion) {
		this.descripcion = descripcion;
		this.solucion = solucion;
	}

	public TeamDescriptionTxt getDescripcion() {
		return descripcion;
	}

	public TeamSolutionTxt getSolucion() {
		return solucion;
	}
	
	public double similitud(CasoTxt c2) 
	{
		double sim_des = this.descripcion.similitud(c2.descripcion);
		return sim_des;
	}

}
