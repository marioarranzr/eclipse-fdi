package CBRtxt;


public class TeamSolutionTxt

{
	private int porteros;
	private int defensas;
	private int delanterosDef;
	private int delanterosOfe;
	
	//Atributos que queremos guardar tenemos que hacer el get y set
	public TeamSolutionTxt(int porteros, int defensas, int delanterosDef, int delanterosOfe)
	{
		this.porteros = porteros;
		this.defensas = defensas;
		this.delanterosDef = delanterosDef;
		this.delanterosOfe = delanterosOfe;
	}

	public int getPorteros() {
		return porteros;
	}

	public void setPorteros(int porteros) {
		this.porteros = porteros;
	}

	public int getDefensas() {
		return defensas;
	}

	public void setDefensas(int defensas) {
		this.defensas = defensas;
	}

	public int getDelanterosDef() {
		return delanterosDef;
	}

	public void setDelanterosDef(int delanterosDef) {
		this.delanterosDef = delanterosDef;
	}

	public int getDelanterosOfe() {
		return delanterosOfe;
	}

	public void setDelanterosOfe(int delanterosOfe) {
		this.delanterosOfe = delanterosOfe;
	}
	
	
	
	
}
