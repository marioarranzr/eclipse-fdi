package CBRtxt;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class archivoTxt 
{
	private static String nombreArchivo;
	private ArrayList<CasoTxt> lista = new ArrayList<CasoTxt>();
	
	public archivoTxt(String archivo)
	{
		nombreArchivo = archivo;
	}
	
	/*********************************************************************************************************/
	public double similitud(CasoTxt c1, CasoTxt c2)
	{
		/*1 es el nuevo caso, 2 el de la _BBDD*/
		double res = c1.similitud(c2);
		return res;
	}
	
	
	/*Para escribir en el fichero de texto*/
	public void escribir()
	{
		File f = new File(nombreArchivo);
		
		try{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);
			for (int i = 0; i < lista.size(); i++) {
				CasoTxt c = lista.get(i);
				TeamDescriptionTxt d = c.getDescripcion();
				wr.write(d.getPorteros()+","+d.getDefensas()+","+d.getDelanterosDef()+","+d.getDelanterosOfe()+","+d.getTiempo()+","+d.getTiempoRestante()+","+d.getGolesFavor()+","+d.getGolesContra());
				TeamSolutionTxt s = c.getSolucion();
				wr.append(","+s.getPorteros()+","+s.getDefensas()+","+s.getDelanterosDef()+","+s.getDelanterosOfe());
				if(i != lista.size()-1)
				{
					wr.append(";");
				}
			}
			wr.close();
			bw.close();
		}catch(IOException e)
		{
			System.out.println(e);
		};
		 
	}
	
	public void leer()
	{
		lista = new ArrayList<CasoTxt>();
		try{
            // Abrimos el archivo
            FileInputStream fstream = new FileInputStream(nombreArchivo);
            // Creamos el objeto de entrada
            DataInputStream entrada = new DataInputStream(fstream);
            // Creamos el Buffer de Lectura
            BufferedReader buffer = new BufferedReader(new InputStreamReader(entrada));
            String strLinea;
            // Leer el archivo linea por linea
            while ((strLinea = buffer.readLine()) != null)   {
                // Imprimimos la l�nea por pantalla
                //System.out.println ("lee " +strLinea);
            	String[] arrayCasos = strLinea.split(";");
            	for (int i = 0; i < arrayCasos.length; i++) 
            	{
            		String[] arrayParametros = arrayCasos[i].split(",");
            		TeamDescriptionTxt descrip = new TeamDescriptionTxt(Integer.parseInt(arrayParametros[0]),Integer.parseInt(arrayParametros[1]),Integer.parseInt(arrayParametros[2]),Integer.parseInt(arrayParametros[3]),Long.parseLong(arrayParametros[4]),Long.parseLong(arrayParametros[5]),Integer.parseInt(arrayParametros[6]),Integer.parseInt(arrayParametros[7]));
                	TeamSolutionTxt sol = new TeamSolutionTxt(Integer.parseInt(arrayParametros[8]),Integer.parseInt(arrayParametros[9]),Integer.parseInt(arrayParametros[10]),Integer.parseInt(arrayParametros[11]));
                	
                	CasoTxt c = new CasoTxt(descrip,sol);
                	lista.add(c);
				}
            	
            }
            // Cerramos el archivo
            entrada.close();
        }catch (Exception e){ //Catch de excepciones
            System.err.println("Ocurrio un error: " + e.getMessage());
        }
		
		for (int i = 0; i < lista.size(); i++) {
			System.out.println (lista.get(i));
		}
	}

	public TeamSolutionTxt compararCasos(CasoTxt c) 
	{
		double parecido = 0;
		double sim_caso_mas_parecido = 0.1;
		CasoTxt caso_mas_parecido = null;
		for (int i = 0; i < lista.size(); i++) 
		{
			parecido = similitud(c, lista.get(i));
			if (sim_caso_mas_parecido<parecido) 
			{
				sim_caso_mas_parecido = parecido;
				caso_mas_parecido = lista.get(i);
			}
		}	
		if (sim_caso_mas_parecido != 1 || lista.size()<26)//Solo vamos a guardar 25
		{
			lista.add(c);
		}
		if (sim_caso_mas_parecido >=0.5)
		{
			return caso_mas_parecido.getSolucion();
		}
		else
		{
			return null;
		}
		
	}
}
