package CBR;

import jcolibri.exception.NoApplicableSimilarityFunctionException;
import jcolibri.method.retrieve.NNretrieval.similarity.LocalSimilarityFunction;

public class Goles implements LocalSimilarityFunction{

	@Override
	public double compute(Object caseObject, Object queryObject) throws NoApplicableSimilarityFunctionException {
		if ((((Integer)caseObject == 0) && ((Integer)queryObject==0)) || (((Integer)caseObject>0) && ((Integer)queryObject>0)) || (((Integer)caseObject<0) && ((Integer)queryObject<0))) 
		{
			return 0.45;
		}
		/*(1 gana o 1 pierde) y 2 empate*/
		else if ((((Integer)caseObject>0) || ((Integer)caseObject>0)) && ((Integer)queryObject == 0))
		{
			return (0.3 - (0.1*Math.abs((Integer)caseObject)));
		}
		/*(2 gana o 2 pierde) y 1 empate*/
		else if ((((Integer)queryObject>0) || ((Integer)queryObject>0)) && ((Integer)caseObject == 0))
		{
			return (0.3 - (0.1*Math.abs((Integer)queryObject)));
		}
		else return 0;
	}

	@Override
	public boolean isApplicable(Object caseObject, Object queryObject) {
		return ((caseObject instanceof Integer) && (queryObject instanceof Integer));
	}

}
