package CBR;


import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class TeamDescription implements CaseComponent
{
	private String caseId;
	private Integer porteros;
	private Integer defensas;
	private Integer delanterosDef;
	private Integer delanterosOfe;
	private Integer tiempo;
	private Integer goles;

	@Override
	public Attribute getIdAttribute() 
	{		
		return new Attribute("caseId", TeamDescription.class);
	}



	public String getCaseId() {
		return caseId;
	}



	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}



	public Integer getPorteros() {
		return porteros;
	}



	public void setPorteros(Integer porteros) {
		this.porteros = porteros;
	}





	public Integer getDefensas() {
		return defensas;
	}





	public void setDefensas(Integer defensas) {
		this.defensas = defensas;
	}





	public Integer getDelanterosDef() {
		return delanterosDef;
	}





	public void setDelanterosDef(Integer delanterosDef) {
		this.delanterosDef = delanterosDef;
	}





	public Integer getDelanterosOfe() {
		return delanterosOfe;
	}





	public void setDelanterosOfe(Integer delanterosOfe) {
		this.delanterosOfe = delanterosOfe;
	}





	public Integer getTiempo() {
		return tiempo;
	}





	public void setTiempo(Integer tiempo) {
		this.tiempo = tiempo;
	}





	public Integer getGoles() {
		return goles;
	}




	
	public void setGoles(Integer goles) {
		this.goles = goles;
		
	}

}
