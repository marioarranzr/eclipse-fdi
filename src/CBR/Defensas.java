package CBR;

import jcolibri.exception.NoApplicableSimilarityFunctionException;
import jcolibri.method.retrieve.NNretrieval.similarity.LocalSimilarityFunction;

public class Defensas implements LocalSimilarityFunction{

	@Override
	public double compute(Object caseObject, Object queryObject) throws NoApplicableSimilarityFunctionException {
		return (0.03 - ((0.01) * Math.abs((Integer)caseObject - (Integer)queryObject)));
	}

	@Override
	public boolean isApplicable(Object caseObject, Object queryObject) {
		return ((caseObject instanceof Integer) && (queryObject instanceof Integer));
	}

}
