package CBR;

import java.io.*;
import java.util.Collection;
import java.util.Iterator;




import java.util.Scanner;

import jcolibri.casebase.LinealCaseBase;
import jcolibri.cbraplications.StandardCBRApplication;
import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRCaseBase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.cbrcore.Connector;
import jcolibri.connector.DataBaseConnector;
import jcolibri.exception.ExecutionException;
import jcolibri.method.retrieve.RetrievalResult;
import jcolibri.method.retrieve.NNretrieval.NNConfig;
import jcolibri.method.retrieve.NNretrieval.NNScoringMethod;
import jcolibri.method.retrieve.NNretrieval.similarity.global.Average;
import jcolibri.method.retrieve.selection.SelectCases;

public class CBR implements StandardCBRApplication
{
	//Connector object
	Connector _connector;
	//CaseBase object
	CBRCaseBase _caseBase;
	TeamSolucion sol = null;

	public TeamSolucion getSol() {
		return sol;
	}

	@Override
	public void configure() throws ExecutionException 
	{
		try
		{
			//Crear el conector con la base de casos
			_connector = new DataBaseConnector();
			//Inicializar el conector con su archivo xml de configuración
			_connector.initFromXMLfile(jcolibri.util.FileIO.findFile("CBR/databaseconfig.xml"));
			//La organizacion en memoria sera lineal
			_caseBase = new LinealCaseBase();
		}
		catch (Exception e)
		{
			throw new ExecutionException(e);
		}
	}

	@Override
	public CBRCaseBase preCycle() throws ExecutionException 
	{
		//Cargar los casos desde el conector a la base de casos
		_caseBase.init(_connector);
		//Imprimir los casos leidos, eliminar cuando funcione
		java.util.Collection<CBRCase> cases = _caseBase.getCases();
		
		return _caseBase;
	}

	@Override
	public void cycle(CBRQuery query) throws ExecutionException 
	{
		//Para configurar el KNN se utiliza un objecto NNconfig
		NNConfig simConfig = new NNConfig();
		//Fijamos la funcion de similitud global
		simConfig.setDescriptionSimFunction(new Average());
		
		//Fijamos las funciones de similitud locales
		simConfig.addMapping(new Attribute("porteros",TeamDescription.class), new Porteros());
		simConfig.addMapping(new Attribute("defensas",TeamDescription.class), new Defensas());
		simConfig.addMapping(new Attribute("delanterosDef",TeamDescription.class), new DelanterosDef());
		simConfig.addMapping(new Attribute("delanterosOfe",TeamDescription.class), new DelanterosOfe());
		simConfig.addMapping(new Attribute("tiempo",TeamDescription.class), new Tiempo());
		simConfig.addMapping(new Attribute("goles",TeamDescription.class), new Goles());
		
		
		
		//Ejecutamos la recuperación por vecino mas proximo
		Collection<RetrievalResult> eval = NNScoringMethod.evaluateSimilarity(_caseBase.getCases(), query, simConfig);
		
		//Seleccionamos los k mejores casos
		eval = SelectCases.selectTopKRR(eval, 1);
		
		//Codigo para adaptar la solucion
		
		 Iterator<RetrievalResult> it = eval.iterator();
		while (it.hasNext())
		{
			RetrievalResult caso = it.next();
			sol = (TeamSolucion) caso.get_case().getSolution();
		}
		TeamDescription d = (TeamDescription) query.getDescription();
		try {
			escribir(d.getPorteros(), d.getDefensas(), d.getDelanterosDef(), d.getDelanterosOfe(), d.getTiempo(), d.getGoles(), sol.getSOLporteros(), sol.getSOLdefensas(), sol.getSOLdelanterosDef(), sol.getSOLdelanterosOfe());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void postCycle() throws ExecutionException 
	{
		this._caseBase.close();
	}
	
	
	public void escribir(int porteros, int defensas, int delanterosDef, int delanterosOfe, int tiempo, int goles, int SOLporteros, int SOLdefensas, int SOLdelanterosDef, int SOLdelanterosOfe) throws IOException{
		
		File input = new File("src/CBR/CBR.sql"); 
		Scanner iterate = new Scanner(input); 
		int id=0; 
		while(iterate.hasNextLine()) { 
			String linea=iterate.nextLine(); 
			id++; 
			} 
		id = id - 1;
		String nomArchivo = "src/CBR/CBR.sql"; 
		FileWriter fr = new FileWriter(nomArchivo,true); 
		BufferedWriter archivo = new BufferedWriter(fr); 
		archivo.newLine();
		archivo.write("insert into CBR values('" + id + "'," + porteros + "," + defensas + "," + delanterosDef + "," + delanterosOfe + "," + tiempo + "," + goles + "," + SOLporteros + "," + SOLdefensas + "," + SOLdelanterosDef + "," + SOLdelanterosOfe + ");");
		archivo.close();
	}

}
