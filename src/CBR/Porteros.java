package CBR;

import jcolibri.exception.NoApplicableSimilarityFunctionException;
import jcolibri.method.retrieve.NNretrieval.similarity.LocalSimilarityFunction;

public class Porteros implements LocalSimilarityFunction
{

	@Override
	public double compute(Object caseObject, Object queryObject) throws NoApplicableSimilarityFunctionException 
	{
		if (caseObject == queryObject)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}

	@Override
	public boolean isApplicable(Object caseObject, Object queryObject) 
	{
		
		return ((caseObject instanceof Integer) && (queryObject instanceof Integer));
	}
	
}
