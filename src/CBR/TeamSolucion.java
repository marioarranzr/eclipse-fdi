package CBR;


import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class TeamSolucion implements CaseComponent
{
	private String caseId;
	private int SOLporteros;
	private int SOLdefensas;
	private int SOLdelanterosDef;
	private int SOLdelanterosOfe;
	
	@Override
	public Attribute getIdAttribute() 
	{		
		return new Attribute("caseId", TeamSolucion.class);
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public int getSOLporteros() {
		return SOLporteros;
	}

	public void setSOLporteros(int sOLporteros) {
		SOLporteros = sOLporteros;
	}

	public int getSOLdefensas() {
		return SOLdefensas;
	}

	public void setSOLdefensas(int sOLdefensas) {
		SOLdefensas = sOLdefensas;
	}

	public int getSOLdelanterosDef() {
		return SOLdelanterosDef;
	}

	public void setSOLdelanterosDef(int sOLdelanterosDef) {
		SOLdelanterosDef = sOLdelanterosDef;
	}

	public int getSOLdelanterosOfe() {
		return SOLdelanterosOfe;
	}

	public void setSOLdelanterosOfe(int sOLdelanterosOfe) {
		SOLdelanterosOfe = sOLdelanterosOfe;
	}

	

}
