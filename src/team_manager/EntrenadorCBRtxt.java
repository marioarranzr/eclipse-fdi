package team_manager;

import java.util.ArrayList;

import comportamiento.AtacanteDefensivo;
import comportamiento.AtacanteOfensivo;
import comportamiento.BloqueaPortero;
import comportamiento.ColocarPortero;
import comportamiento.DefensaCentral;
import comportamiento.DefensaDerecha;
import comportamiento.DefensaIzquierda;
import comportamiento.DespejarCen;
import comportamiento.DespejarDrc;
import comportamiento.DespejarIzq;
import comportamiento.DespejarPortero;
import comportamiento.EnPorteria;
import comportamiento.MoverseCen;
import comportamiento.MoverseDrc;
import comportamiento.MoverseIzq;
import comportamiento.PresionarDefensivo;
import comportamiento.PresionarOfensivo;
import comportamiento.Quieto;


import CBRtxt.CasoTxt;
import CBRtxt.TeamDescriptionTxt;
import CBRtxt.TeamSolutionTxt;
import CBRtxt.archivoTxt;

import teams.ucmTeam.*;

public class EntrenadorCBRtxt extends TeamManager {

	Behaviour[] behav;
	String[] comportamiento;
	private int numPorteros;
	private int numDefensas;
	private int numDelanterosDef;
	private int numDelanterosOfe;
	private archivoTxt fich;
	private boolean flag = false;
 
	@Override
	public Behaviour[] createBehaviours() {

		fich = new archivoTxt("src/t1314grupo08/CBR.txt");
		fich.leer();
		
		numPorteros=1;
		numDefensas=0;
		numDelanterosDef=4;
		numDelanterosOfe=0;
		
		behav = new Behaviour[5];
		comportamiento = new String[5];
		behav[0] = new EnPorteria();
		comportamiento[0] = "EnPorteria";

		/*Delantero defensivo*/
		behav[1] = new AtacanteDefensivo();
		comportamiento[1]= "AtacanteDefensivo";
		
		/*Delantero defensivo*/
		behav[2] = new AtacanteDefensivo();
		comportamiento[2]= "AtacanteDefensivo";
		
		/*Delantero defensivo*/
		behav[3] = new AtacanteDefensivo();
		comportamiento[3]= "AtacanteDefensivo";
		
		/*Delantero defensivo*/
		behav[4] = new AtacanteDefensivo();
		comportamiento[4]= "AtacanteDefensivo";
		
		return behav;
	}

	@Override
	public Behaviour getDefaultBehaviour(int arg0) {
		return getBehaviour(arg0);

	}

	@Override
	public int onConfigure() {
		return RobotAPI.ROBOT_OK;
	}

	@Override
	protected void onTakeStep() {
		
		RobotAPI robot = _players[0].getRobotAPI();
		double x = robot.getTimeStamp();
		double y = robot.getMatchTotalTime()*9/10;
		if (x > y && !flag)
		{
			fich.escribir();
			flag = true;
		}
		if (robot.getJustScored()!=0)
		{
			/*CBR*/
			/*Crear el caso nuevo, Descripcion y Solucion*/
			
			TeamDescriptionTxt d = new TeamDescriptionTxt(numPorteros, numDefensas, numDelanterosDef, numDelanterosOfe, robot.getMatchTotalTime(), robot.getMatchRemainingTime(), robot.getMyScore(),robot.getOpponentScore());
			ArrayList<Integer> sol = sacar_solucion();
			TeamSolutionTxt s = new TeamSolutionTxt(sol.get(0), sol.get(1), sol.get(2), sol.get(3));
			CasoTxt c = new CasoTxt(d, s);
			
			/*Comparamos si hay algun caso parecido y devolvemos la solucion*/
			TeamSolutionTxt solucion = fich.compararCasos(c);
			if (solucion != null)
			{
				numPorteros = solucion.getPorteros();
				numDefensas = solucion.getDefensas();
				numDelanterosDef= solucion.getDelanterosDef();
				numDelanterosOfe= solucion.getDelanterosOfe();
				cambiar_comportamientos();
			}
		}
		else
		{
		int estadoActual;
		int estadoSiguiente;
		for (int i=0; i<5; i++)
		{
			String comportamientos = comportamiento[i];
			switch (comportamientos)
			{
				case "AtacanteOfensivo" :
				{
					AtacanteOfensivo a = (AtacanteOfensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarCen" :
				{
					DespejarCen a = (DespejarCen)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "AtacanteDefensivo" :
				{
					AtacanteDefensivo a = (AtacanteDefensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "EnPorteria" :
				{
					EnPorteria a = (EnPorteria)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaCentral" :
				{
					DefensaCentral a = (DefensaCentral)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaDerecha" :
				{
					DefensaDerecha a = (DefensaDerecha)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaIzquierda" :
				{
					DefensaIzquierda a = (DefensaIzquierda)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseCen" :
				{
					MoverseCen a = (MoverseCen)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "PresionarDefensivo" :
				{
					PresionarDefensivo a = (PresionarDefensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "PresionarOfensivo" :
				{
					PresionarOfensivo a = (PresionarOfensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "ColocarPortero" :
				{
					ColocarPortero a = (ColocarPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarPortero" :
				{
					DespejarPortero a = (DespejarPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseDrc" :
				{
					MoverseDrc a = (MoverseDrc)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseIzq" :
				{
					MoverseIzq a = (MoverseIzq)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarDrc" :
				{
					DespejarDrc a = (DespejarDrc)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarIzq" :
				{
					DespejarIzq a = (DespejarIzq)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "BloqueaPortero" :
				{
					BloqueaPortero a = (BloqueaPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				default :
				{
					Quieto a = (Quieto)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
			}
			
			if (estadoActual!=estadoSiguiente)
			{
				switch (estadoSiguiente)
				{
					case 0 :
					{
						_behaviours[i] = new AtacanteOfensivo();
						comportamiento[i]= "AtacanteOfensivo";
						break;
					}
					case 1 :
					{
						_behaviours[i] = new DespejarCen() ;
						comportamiento[i]= "DespejarCen" ;
						break;
					}
					case 2 :
					{
						_behaviours[i] = new AtacanteDefensivo() ;
						comportamiento[i]= "AtacanteDefensivo";
						break;
					}
					case 3 :
					{
						_behaviours[i] = new EnPorteria();
						comportamiento[i]= "EnPorteria";
						break;
					}
					case 4 :
					{
						_behaviours[i] = new DefensaCentral();
						comportamiento[i]= "DefensaCentral";
						break;
					}
					case 5 :
					{
						_behaviours[i] = new DefensaDerecha();
						comportamiento[i]= "DefensaDerecha";
						break;
					}
					case 6 :
					{
						_behaviours[i] = new DefensaIzquierda();
						comportamiento[i]= "DefensaIzquierda";
						break;
					}
					case 7 :
					{
						_behaviours[i] = new MoverseCen();
						comportamiento[i]= "MoverseCen";
						break;
					}
					case 8 :
					{
						_behaviours[i] = new PresionarDefensivo();
						comportamiento[i]= "PresionarDefensivo";
						break;
					}
					case 9 :
					{
						_behaviours[i] = new PresionarOfensivo();
						comportamiento[i]= "PresionarOfensivo";
						break;
					}
					case 10 :
					{
						_behaviours[i] = new Quieto();
						comportamiento[i]= "Quieto";
						break;
					}
					case 11 :
					{
						_behaviours[i] = new ColocarPortero();
						comportamiento[i]= "ColocarPortero";
						break;
					}
					case 12 :
					{
						_behaviours[i] = new DespejarPortero();
						comportamiento[i]= "DespejarPortero";
						break;
					}
					case 13 :
					{
						_behaviours[i] = new MoverseDrc();
						comportamiento[i]= "MoverseDrc";
						break;
					}
					case 14 :
					{
						_behaviours[i] = new MoverseIzq();
						comportamiento[i]= "MoverseIzq";
						break;
					}
					case 15 :
					{
						_behaviours[i] = new DespejarDrc();
						comportamiento[i]= "DespejarDrc";
						break;
					}
					case 16 :
					{
						_behaviours[i] = new DespejarIzq();
						comportamiento[i]= "DespejarIzq";
						break;
					}
					case 17 :
					{
						_behaviours[i] = new BloqueaPortero();
						comportamiento[i]= "BloqueaPortero";
						break;
					}
				}
			}
			_players[i].setBehaviour(_behaviours[i]);
		}
	}
		}

	private ArrayList<Integer> sacar_solucion() 
	{
		/*Guardamos : Porteros, Defensas, DelanteroDef, DelanteroOfe*/
		ArrayList<Integer> solucion = new ArrayList<Integer>();
		RobotAPI robot = _players[0].getRobotAPI();
		int goles_favor = robot.getMyScore();
		int goles_contra = robot.getOpponentScore();
		long tiempo_total = robot.getMatchTotalTime();
		long tiempo = robot.getTimeStamp();
		int dif = goles_favor - goles_contra;
		
		///////////////ESTRATEGIA///////////////////
		if (tiempo >= (tiempo_total*7.5/10)){
			if (dif == 1){
				/*Porteros*/
				solucion.add(1);
				/*Defensas*/
				solucion.add(3);
				/*DelanteroDef*/
				solucion.add(1);
				/*DelanteroOfe*/
				solucion.add(0);	
				
			} else if (dif <= 0){
				/*Porteros*/
				solucion.add(1);
				/*Defensas*/
				solucion.add(1);
				/*DelanteroDef*/
				solucion.add(1);
				/*DelanteroOfe*/
				solucion.add(2);
			}
		} else {
			if (dif >= 1){
				/*Porteros*/
				solucion.add(1);
				/*Defensas*/
				solucion.add(3);
				/*DelanteroDef*/
				solucion.add(1);
				/*DelanteroOfe*/
				solucion.add(0);
				
			} else if (dif == 0){
				/*Porteros*/
				solucion.add(1);
				/*Defensas*/
				solucion.add(2);
				/*DelanteroDef*/
				solucion.add(1);
				/*DelanteroOfe*/
				solucion.add(1);
				
			} else if (dif < 0){
				/*Porteros*/
				solucion.add(1);
				/*Defensas*/
				solucion.add(1);
				/*DelanteroDef*/
				solucion.add(2);
				/*DelanteroOfe*/
				solucion.add(1);
			}
		}
		return solucion;
	}
		

	private void cambiar_comportamientos() 
	{
		if (numPorteros == 1)
		{
			_behaviours[0] = new EnPorteria();
			comportamiento[0]= "EnPorteria";
			_players[0].setBehaviour(_behaviours[0]);
			if (numDefensas == 0)
			{
				if (numDelanterosDef == 4)
				{
					/*Delantero defensivo*/
					_behaviours[1] = new AtacanteDefensivo();
					comportamiento[1]= "AtacanteDefensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero defensivo*/
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else if (numDelanterosDef == 3)
				{
					/*Delantero defensivo*/
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
				}
				else if (numDelanterosDef == 2)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
				}
				else if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			}
			else if (numDefensas == 1)
			{
				/*Defensa central*/
				_behaviours[1] = new DefensaCentral();
				comportamiento[1]= "DefensaCentral";
				_players[1].setBehaviour(_behaviours[1]);
				
				if (numDelanterosDef == 3)
				{
					/*Delantero defensivo*/
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else if (numDelanterosDef == 2)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
				}
				else if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			}
			else if (numDefensas == 2)
			{
				/*Defensa derecho*/
				_behaviours[2] = new DefensaDerecha();
				comportamiento[2]= "DefensaDerecha";
				_players[2].setBehaviour(_behaviours[2]);
				/*Defensa izquierdo*/
				_behaviours[1] = new DefensaIzquierda();
				comportamiento[1]= "DefensaIzquierda";
				_players[1].setBehaviour(_behaviours[1]);
				
				if (numDelanterosDef == 2)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			}
			else
			{
				/*Defensa derecho*/
				_behaviours[2] = new DefensaDerecha();
				comportamiento[2]= "DefensaDerecha";
				_players[2].setBehaviour(_behaviours[2]);
				/*Defensa izquierdo*/
				_behaviours[4] = new DefensaIzquierda();
				comportamiento[4]= "DefensaIzquierda";
				_players[4].setBehaviour(_behaviours[4]);
				/*Defensa central*/
				_behaviours[1] = new DefensaCentral();
				comportamiento[1]= "DefensaCentral";
				_players[1].setBehaviour(_behaviours[1]);
				
				if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
				}
			}
		}
	}
}
