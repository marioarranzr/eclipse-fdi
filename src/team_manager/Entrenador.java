package team_manager;

import comportamiento.AtacanteDefensivo;
import comportamiento.AtacanteOfensivo;
import comportamiento.ColocarPortero;
import comportamiento.DefensaCentral;
import comportamiento.DefensaDerecha;
import comportamiento.DefensaIzquierda;
import comportamiento.DespejarCen;
import comportamiento.DespejarDrc;
import comportamiento.DespejarIzq;
import comportamiento.DespejarPortero;
import comportamiento.EnPorteria;
import comportamiento.MoverseCen;
import comportamiento.MoverseDrc;
import comportamiento.MoverseIzq;
import comportamiento.PresionarDefensivo;
import comportamiento.PresionarOfensivo;
import comportamiento.Quieto;

import teams.ucmTeam.*;

public class Entrenador extends TeamManager {

	Behaviour[] behav;
	String[] comportamiento;
 
	@Override
	public Behaviour[] createBehaviours() {

		behav = new Behaviour[5];
		comportamiento = new String[5];
		behav[0] = new EnPorteria();
		comportamiento[0] = "EnPorteria";
		behav[1] = new DefensaIzquierda();
		comportamiento[1] = "DefensaIzquierda";
		behav[2] = new DefensaDerecha();
		comportamiento[2] = "DefensaDerecha";
		behav[3] = new AtacanteDefensivo();
		comportamiento[3] = "AtacanteDefensivo";
		behav[4] = new AtacanteOfensivo();
		comportamiento[4] = "AtacanteOfensivo";
		return behav;
	}

	@Override
	public Behaviour getDefaultBehaviour(int arg0) {
		return getBehaviour(arg0);

	}

	@Override
	public int onConfigure() {
		return RobotAPI.ROBOT_OK;
	}

	@Override
	protected void onTakeStep() {
		int estadoActual;
		int estadoSiguiente;
		
		//////////////////ESTRATEGIA/////////////////////////
		
		RobotAPI robot = _players[0].getRobotAPI();
		
		if (robot.getJustScored()!= 0){
			int goles_favor = robot.getMyScore();
			int goles_contra = robot.getOpponentScore();
			long tiempo_total = robot.getMatchTotalTime();
			long tiempo = robot.getTimeStamp();
			int dif = goles_favor - goles_contra;
			if (tiempo >= (tiempo_total*7.5/10)){
				if (dif == 1){
					_behaviours[0] = new EnPorteria();
					comportamiento[0]= "EnPorteria";
					_players[0].setBehaviour(_behaviours[0]);
					
					_behaviours[1] = new DefensaCentral();
					comportamiento[1]= "DefensaCentral";
					_players[1].setBehaviour(_behaviours[1]);
					
					_behaviours[2] = new DefensaDerecha();
					comportamiento[2]= "DefensaDerecha";
					_players[2].setBehaviour(_behaviours[2]);
					
					_behaviours[3] = new DefensaIzquierda();
					comportamiento[3]= "DefensaIzquierda";
					_players[3].setBehaviour(_behaviours[3]);
					
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
					
				} else if (dif <= 0){
					_behaviours[0] = new EnPorteria();
					comportamiento[0]= "EnPorteria";
					_players[0].setBehaviour(_behaviours[0]);
					
					_behaviours[1] = new DefensaCentral();
					comportamiento[1]= "DefensaCentral";
					_players[1].setBehaviour(_behaviours[1]);
					
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
					
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			} else {
				if (dif == 2){
					_behaviours[0] = new EnPorteria();
					comportamiento[0]= "EnPorteria";
					_players[0].setBehaviour(_behaviours[0]);
					
					_behaviours[1] = new DefensaCentral();
					comportamiento[1]= "DefensaCentral";
					_players[1].setBehaviour(_behaviours[1]);
					
					_behaviours[2] = new DefensaDerecha();
					comportamiento[2]= "DefensaDerecha";
					_players[2].setBehaviour(_behaviours[2]);
					
					_behaviours[3] = new DefensaIzquierda();
					comportamiento[3]= "DefensaIzquierda";
					_players[3].setBehaviour(_behaviours[3]);
					
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
					
				} else if (dif == 0){
					_behaviours[0] = new EnPorteria();
					comportamiento[0]= "EnPorteria";
					_players[0].setBehaviour(_behaviours[0]);
					
					_behaviours[1] = new DefensaIzquierda();
					comportamiento[1]= "DefensaIzquierda";
					_players[1].setBehaviour(_behaviours[1]);
					
					_behaviours[2] = new DefensaDerecha();
					comportamiento[2]= "DefensaDerecha";
					_players[2].setBehaviour(_behaviours[2]);
					
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
					
				} else if (dif == -1){
					_behaviours[0] = new EnPorteria();
					comportamiento[0]= "EnPorteria";
					_players[0].setBehaviour(_behaviours[0]);
					
					_behaviours[1] = new DefensaCentral();
					comportamiento[1]= "DefensaCentral";
					_players[1].setBehaviour(_behaviours[1]);
					
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			}
		}

		///////////////////////////////////////////
		
		
		for (int i=0; i<5; i++)
		{
			String comportamientos = comportamiento[i];
			switch (comportamientos)
			{
				case "AtacanteOfensivo" :
				{
					AtacanteOfensivo a = (AtacanteOfensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarCen" :
				{
					DespejarCen a = (DespejarCen)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "AtacanteDefensivo" :
				{
					AtacanteDefensivo a = (AtacanteDefensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "EnPorteria" :
				{
					EnPorteria a = (EnPorteria)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaCentral" :
				{
					DefensaCentral a = (DefensaCentral)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaDerecha" :
				{
					DefensaDerecha a = (DefensaDerecha)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaIzquierda" :
				{
					DefensaIzquierda a = (DefensaIzquierda)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseCen" :
				{
					MoverseCen a = (MoverseCen)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "PresionarDefensivo" :
				{
					PresionarDefensivo a = (PresionarDefensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "PresionarOfensivo" :
				{
					PresionarOfensivo a = (PresionarOfensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "ColocarPortero" :
				{
					ColocarPortero a = (ColocarPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarPortero" :
				{
					DespejarPortero a = (DespejarPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseDrc" :
				{
					MoverseDrc a = (MoverseDrc)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseIzq" :
				{
					MoverseIzq a = (MoverseIzq)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarDrc" :
				{
					DespejarDrc a = (DespejarDrc)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarIzq" :
				{
					DespejarIzq a = (DespejarIzq)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				default :
				{
					Quieto a = (Quieto)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
			}
			
			if (estadoActual!=estadoSiguiente)
			{
				switch (estadoSiguiente)
				{
					case 0 :
					{
						_behaviours[i] = new AtacanteOfensivo();
						comportamiento[i]= "AtacanteOfensivo";
						break;
					}
					case 1 :
					{
						_behaviours[i] = new DespejarCen() ;
						comportamiento[i]= "DespejarCen" ;
						break;
					}
					case 2 :
					{
						_behaviours[i] = new AtacanteDefensivo() ;
						comportamiento[i]= "AtacanteDefensivo";
						break;
					}
					case 3 :
					{
						_behaviours[i] = new EnPorteria();
						comportamiento[i]= "EnPorteria";
						break;
					}
					case 4 :
					{
						_behaviours[i] = new DefensaCentral();
						comportamiento[i]= "DefensaCentral";
						break;
					}
					case 5 :
					{
						_behaviours[i] = new DefensaDerecha();
						comportamiento[i]= "DefensaDerecha";
						break;
					}
					case 6 :
					{
						_behaviours[i] = new DefensaIzquierda();
						comportamiento[i]= "DefensaIzquierda";
						break;
					}
					case 7 :
					{
						_behaviours[i] = new MoverseCen();
						comportamiento[i]= "MoverseCen";
						break;
					}
					case 8 :
					{
						_behaviours[i] = new PresionarDefensivo();
						comportamiento[i]= "PresionarDefensivo";
						break;
					}
					case 9 :
					{
						_behaviours[i] = new PresionarOfensivo();
						comportamiento[i]= "PresionarOfensivo";
						break;
					}
					case 10 :
					{
						_behaviours[i] = new Quieto();
						comportamiento[i]= "Quieto";
						break;
					}
					case 11 :
					{
						_behaviours[i] = new ColocarPortero();
						comportamiento[i]= "ColocarPortero";
						break;
					}
					case 12 :
					{
						_behaviours[i] = new DespejarPortero();
						comportamiento[i]= "DespejarPortero";
						break;
					}
					case 13 :
					{
						_behaviours[i] = new MoverseDrc();
						comportamiento[i]= "MoverseDrc";
						break;
					}
					case 14 :
					{
						_behaviours[i] = new MoverseIzq();
						comportamiento[i]= "MoverseIzq";
						break;
					}
					case 15 :
					{
						_behaviours[i] = new DespejarDrc();
						comportamiento[i]= "DespejarDrc";
						break;
					}
					case 16 :
					{
						_behaviours[i] = new DespejarIzq();
						comportamiento[i]= "DespejarIzq";
						break;
					}
				}
			}
			_players[i].setBehaviour(_behaviours[i]);
		}
	}
}
