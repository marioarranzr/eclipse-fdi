package team_manager;

import CBR.CBR;
import CBR.HSQLDBserver2;
import CBR.TeamDescription;
import CBR.TeamSolucion;
import comportamiento.AtacanteDefensivo;
import comportamiento.AtacanteOfensivo;
import comportamiento.BloqueaPortero;
import comportamiento.ColocarPortero;
import comportamiento.DefensaCentral;
import comportamiento.DefensaDerecha;
import comportamiento.DefensaIzquierda;
import comportamiento.DespejarCen;
import comportamiento.DespejarDrc;
import comportamiento.DespejarIzq;
import comportamiento.DespejarPortero;
import comportamiento.EnPorteria;
import comportamiento.MoverseCen;
import comportamiento.MoverseDrc;
import comportamiento.MoverseIzq;
import comportamiento.PresionarDefensivo;
import comportamiento.PresionarOfensivo;
import comportamiento.Quieto;

import jcolibri.cbrcore.CBRQuery;
import teams.ucmTeam.*;

public class EntrenadorCBR extends TeamManager {

	Behaviour[] behav;
	String[] comportamiento;
	private Integer numPorteros;
	private Integer numDefensas;
	private Integer numDelanterosDef;
	private Integer numDelanterosOfe;

 
	@Override
	public Behaviour[] createBehaviours() {

		
		numPorteros=1;
		numDefensas=2;
		numDelanterosDef=1;
		numDelanterosOfe=1;
		
		behav = new Behaviour[5];
		comportamiento = new String[5];
		behav[0] = new EnPorteria();
		comportamiento[0]= "EnPorteria";
		behav[1] = new DefensaIzquierda();
		comportamiento[1]= "DefensaIzquierda";
		behav[2] = new DefensaDerecha();
		comportamiento[2]= "DefensaDerecha";
		behav[3] = new AtacanteOfensivo();
		comportamiento[3]= "AtacanteOfensivo";
		behav[4] = new AtacanteDefensivo();
		comportamiento[4]= "AtacanteDefensivo";
		return behav;
	}

	@Override
	public Behaviour getDefaultBehaviour(int arg0) {
		return getBehaviour(arg0);

	}

	@Override
	public int onConfigure() {
		return RobotAPI.ROBOT_OK;
	}

	@Override
	protected void onTakeStep() {
		
		RobotAPI robot = _players[0].getRobotAPI();
		
		if (robot.getJustScored()!=0)
		{
			//Lanzar el SGBD
			HSQLDBserver2.init();
			
			//Crear el objeto que implementa la aplicacion CBR
			CBR cbr = new CBR();
			try
			{
				//Configuración
				cbr.configure();
				//preciclo
				cbr.preCycle();
				
				//Crear objeto que almacena la consulta
				CBRQuery query  = new CBRQuery();
				TeamDescription d  = new TeamDescription();
				d.setPorteros(numPorteros);
				d.setDefensas(numDefensas);
				d.setDelanterosDef(numDelanterosDef);
				d.setDelanterosOfe(numDelanterosOfe);
				Integer tiempo = sacar_parte_tiempo((robot.getMatchRemainingTime()*100)/robot.getMatchTotalTime());
				d.setTiempo(tiempo);
				Integer goles = (Integer)(robot.getMyScore()-robot.getOpponentScore());
				d.setGoles(goles);
				query.setDescription(d);
				
				//Ejecutar el ciclo
				cbr.cycle(query);
				
				//Aplicar la solucion
				TeamSolucion solucion = cbr.getSol();
				numPorteros = solucion.getSOLporteros();
				numDefensas = solucion.getSOLdefensas();
				numDelanterosDef = solucion.getSOLdelanterosDef();
				numDelanterosOfe = solucion.getSOLdelanterosOfe();
				cambiar_comportamientos();
				
				//Ejecutar el posciclo
				cbr.postCycle();
				
			}
			catch (Exception e)
			{
				org.apache.commons.logging.LogFactory.getLog(CBR.class).error(e);
			}
			
			//Apagar el SGBD
			HSQLDBserver2.shutDown();
			
		}
		else
		{
		int estadoActual;
		int estadoSiguiente;
		for (int i=0; i<5; i++)
		{
			String comportamientos = comportamiento[i];
			switch (comportamientos)
			{
				case "AtacanteOfensivo" :
				{
					AtacanteOfensivo a = (AtacanteOfensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarCen" :
				{
					DespejarCen a = (DespejarCen)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "AtacanteDefensivo" :
				{
					AtacanteDefensivo a = (AtacanteDefensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "EnPorteria" :
				{
					EnPorteria a = (EnPorteria)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaCentral" :
				{
					DefensaCentral a = (DefensaCentral)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaDerecha" :
				{
					DefensaDerecha a = (DefensaDerecha)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DefensaIzquierda" :
				{
					DefensaIzquierda a = (DefensaIzquierda)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseCen" :
				{
					MoverseCen a = (MoverseCen)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "PresionarDefensivo" :
				{
					PresionarDefensivo a = (PresionarDefensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "PresionarOfensivo" :
				{
					PresionarOfensivo a = (PresionarOfensivo)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "ColocarPortero" :
				{
					ColocarPortero a = (ColocarPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarPortero" :
				{
					DespejarPortero a = (DespejarPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseDrc" :
				{
					MoverseDrc a = (MoverseDrc)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "MoverseIzq" :
				{
					MoverseIzq a = (MoverseIzq)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarDrc" :
				{
					DespejarDrc a = (DespejarDrc)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "DespejarIzq" :
				{
					DespejarIzq a = (DespejarIzq)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				case "BloqueaPortero" :
				{
					BloqueaPortero a = (BloqueaPortero)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
				default :
				{
					Quieto a = (Quieto)getBehaviour(i);
					estadoActual = a.getMaquina().getEstadoActual();
					estadoSiguiente = a.getMaquina().getEstadoSiguiente();
					break;
				}
			}
			
			if (estadoActual!=estadoSiguiente)
			{
				switch (estadoSiguiente)
				{
					case 0 :
					{
						_behaviours[i] = new AtacanteOfensivo();
						comportamiento[i]= "AtacanteOfensivo";
						break;
					}
					case 1 :
					{
						_behaviours[i] = new DespejarCen() ;
						comportamiento[i]= "DespejarCen" ;
						break;
					}
					case 2 :
					{
						_behaviours[i] = new AtacanteDefensivo() ;
						comportamiento[i]= "AtacanteDefensivo";
						break;
					}
					case 3 :
					{
						_behaviours[i] = new EnPorteria();
						comportamiento[i]= "EnPorteria";
						break;
					}
					case 4 :
					{
						_behaviours[i] = new DefensaCentral();
						comportamiento[i]= "DefensaCentral";
						break;
					}
					case 5 :
					{
						_behaviours[i] = new DefensaDerecha();
						comportamiento[i]= "DefensaDerecha";
						break;
					}
					case 6 :
					{
						_behaviours[i] = new DefensaIzquierda();
						comportamiento[i]= "DefensaIzquierda";
						break;
					}
					case 7 :
					{
						_behaviours[i] = new MoverseCen();
						comportamiento[i]= "MoverseCen";
						break;
					}
					case 8 :
					{
						_behaviours[i] = new PresionarDefensivo();
						comportamiento[i]= "PresionarDefensivo";
						break;
					}
					case 9 :
					{
						_behaviours[i] = new PresionarOfensivo();
						comportamiento[i]= "PresionarOfensivo";
						break;
					}
					case 10 :
					{
						_behaviours[i] = new Quieto();
						comportamiento[i]= "Quieto";
						break;
					}
					case 11 :
					{
						_behaviours[i] = new ColocarPortero();
						comportamiento[i]= "ColocarPortero";
						break;
					}
					case 12 :
					{
						_behaviours[i] = new DespejarPortero();
						comportamiento[i]= "DespejarPortero";
						break;
					}
					case 13 :
					{
						_behaviours[i] = new MoverseDrc();
						comportamiento[i]= "MoverseDrc";
						break;
					}
					case 14 :
					{
						_behaviours[i] = new MoverseIzq();
						comportamiento[i]= "MoverseIzq";
						break;
					}
					case 15 :
					{
						_behaviours[i] = new DespejarDrc();
						comportamiento[i]= "DespejarDrc";
						break;
					}
					case 16 :
					{
						_behaviours[i] = new DespejarIzq();
						comportamiento[i]= "DespejarIzq";
						break;
					}
					case 17 :
					{
						_behaviours[i] = new BloqueaPortero();
						comportamiento[i]= "BloqueaPortero";
						break;
					}
				}
			}
			_players[i].setBehaviour(_behaviours[i]);
		}
	}
		}

	private void cambiar_comportamientos() 
	{
		///////////////ESTRATEGIA////////////////
		if (numPorteros == 1)
		{
			_behaviours[0] = new EnPorteria();
			comportamiento[0]= "EnPorteria";
			_players[0].setBehaviour(_behaviours[0]);
			if (numDefensas == 0)
			{
				if (numDelanterosDef == 4)
				{
					/*Delantero defensivo*/
					_behaviours[1] = new AtacanteDefensivo();
					comportamiento[1]= "AtacanteDefensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero defensivo*/
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else if (numDelanterosDef == 3)
				{
					/*Delantero defensivo*/
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
				}
				else if (numDelanterosDef == 2)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
				}
				else if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[1] = new AtacanteOfensivo();
					comportamiento[1]= "AtacanteOfensivo";
					_players[1].setBehaviour(_behaviours[1]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			}
			else if (numDefensas == 1)
			{
				/*Defensa central*/
				_behaviours[1] = new DefensaCentral();
				comportamiento[1]= "DefensaCentral";
				_players[1].setBehaviour(_behaviours[1]);
				
				if (numDelanterosDef == 3)
				{
					/*Delantero defensivo*/
					_behaviours[2] = new AtacanteDefensivo();
					comportamiento[2]= "AtacanteDefensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else if (numDelanterosDef == 2)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
				}
				else if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[2] = new AtacanteOfensivo();
					comportamiento[2]= "AtacanteOfensivo";
					_players[2].setBehaviour(_behaviours[2]);
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			}
			else if (numDefensas == 2)
			{
				/*Defensa derecho*/
				_behaviours[2] = new DefensaDerecha();
				comportamiento[2]= "DefensaDerecha";
				_players[2].setBehaviour(_behaviours[2]);
				/*Defensa izquierdo*/
				_behaviours[1] = new DefensaIzquierda();
				comportamiento[1]= "DefensaIzquierda";
				_players[1].setBehaviour(_behaviours[1]);
				
				if (numDelanterosDef == 2)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero defensivo*/
					_behaviours[4] = new AtacanteDefensivo();
					comportamiento[4]= "AtacanteDefensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
					/*Delantero ofensivo*/
					_behaviours[4] = new AtacanteOfensivo();
					comportamiento[4]= "AtacanteOfensivo";
					_players[4].setBehaviour(_behaviours[4]);
				}
			}
			else
			{
				/*Defensa derecho*/
				_behaviours[2] = new DefensaDerecha();
				comportamiento[2]= "DefensaDerecha";
				_players[2].setBehaviour(_behaviours[2]);
				/*Defensa izquierdo*/
				_behaviours[4] = new DefensaIzquierda();
				comportamiento[4]= "DefensaIzquierda";
				_players[4].setBehaviour(_behaviours[4]);
				/*Defensa central*/
				_behaviours[1] = new DefensaCentral();
				comportamiento[1]= "DefensaCentral";
				_players[1].setBehaviour(_behaviours[1]);
				
				if (numDelanterosDef == 1)
				{
					/*Delantero defensivo*/
					_behaviours[3] = new AtacanteDefensivo();
					comportamiento[3]= "AtacanteDefensivo";
					_players[3].setBehaviour(_behaviours[3]);
				}
				else
				{
					/*Delantero ofensivo*/
					_behaviours[3] = new AtacanteOfensivo();
					comportamiento[3]= "AtacanteOfensivo";
					_players[3].setBehaviour(_behaviours[3]);
				}
			}
		}
	}
	
	private Integer sacar_parte_tiempo(double cuenta) 
	{
		int c = (int)cuenta;
		if (c<10) return 0;
		else if (c<20) return 1;
		else if (c<30) return 2;
		else if (c<40) return 3;
		else if (c<50) return 4;
		else if (c<60) return 5;
		else if (c<70) return 6;
		else if (c<80) return 7;
		else if (c<90) return 8;
		else return 9;
	}
}
